/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Module  :			Timer								*/
/*   Version :          V01	                                */
/*	 File	 :			config.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef TIMER_CONFIG_H_
#define TIMER_CONFIG_H_




/*	Timer 0 Preload and Overflow Values 		*/
#define TIMER0_u8_INITIAL_PRELOAD						(u8)0
#define TIMER0_u8_INITIAL_OVERFLOWS_COUNT				(u32)62500

#define TIMER1_u8_INITIAL_PRELOAD     	    			(u8)0
#define TIMER1_u8_INITIAL_OVERFLOWS_COUNT      			(u8)1

#define TIMER2_u8_INITIAL_PRELOAD         				(u8)0
#define TIMER2_u8_INITIAL_OVERFLOWS_COUNT      			(u8)1




typedef struct
{
	u8	Prescaler;
	u8 	Mode;
	u8 	InitialState;
	u8  Preload;
	u8 	CTC_value;
	u8 	PWM_DutyCycle;
	u32 OverflowsCount;
	u8 	InterruptEvent;

}Timer_Configurations;


Timer_Configurations Timer_ASTimerConfigsArray[TIMER_u8_NO_OF_TIMERS] = {

		{
				TIMER0_u8_NO_PRESCALER,
				TIMER0_u8_MODE_NORMAL,
				TIMER_u8_STATE_DISABLED,
				TIMER0_u8_INITIAL_PRELOAD,
				100,
				50,
				TIMER0_u8_INITIAL_OVERFLOWS_COUNT,
				TIMER0_u8_OVERFLOW_INTERRUPT
		}
};


#endif /* TIMER_CONFIG_H_ */
