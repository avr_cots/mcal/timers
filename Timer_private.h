/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Module  :			Timer								*/
/*   Version :          V01	                                */
/*	 File	 :			private.h							*/
/*                                                          */
/* ******************************************************** */

#ifndef TIMER_PRIVATE_H_
#define TIMER_PRIVATE_H_

#define TIMER_u8_TIMSK				((Register*)0x59)->ByteAccess
#define TIMER_u8_TIFR				((Register*)0x58)->ByteAccess


#define TIMER_u8_TCCR0              ((Register*)0x53)->ByteAccess
#define TIMER_u8_TCNT0              ((Register*)0x52)->ByteAccess
#define TIMER_u8_OCR0               ((Register*)0x5C)->ByteAccess

#define TIMER_u8_TCCR1A             ((Register*)0x4F)->ByteAccess
#define TIMER_u8_TCCR1B             ((Register*)0x4E)->ByteAccess
#define TIMER_u8_TCNT1H             ((Register*)0x4D)->ByteAccess
#define TIMER_u8_TCNT1L             ((Register*)0x4C)->ByteAccess
#define TIMER_u8_OCR1AH             ((Register*)0x4B)->ByteAccess
#define TIMER_u8_OCR1AL             ((Register*)0x4A)->ByteAccess
#define TIMER_u8_OCR1BH             ((Register*)0x49)->ByteAccess
#define TIMER_u8_OCR1BL             ((Register*)0x48)->ByteAccess
#define TIMER_u8_ICR1H              ((Register*)0x47)->ByteAccess
#define TIMER_u8_ICR1L              ((Register*)0x46)->ByteAccess

#define TIMER_u8_TCCR2              ((Register*)0x45)->ByteAccess
#define TIMER_u8_TCNT2              ((Register*)0x44)->ByteAccess
#define TIMER_u8_OCR2               ((Register*)0x43)->ByteAccess




#define TIMER_u8_NO_OF_TIMERS							(u8)3


/*	Timer 0 Clock Select	*/
#define TIMER0_u8_STOPPED								(u8)0
#define TIMER0_u8_NO_PRESCALER							(u8)1
#define TIMER0_u8_PRESCALER_8							(u8)2
#define TIMER0_u8_PRESCALER_64							(u8)3
#define TIMER0_u8_PRESCALER_256							(u8)4
#define TIMER0_u8_PRESCALER_1024						(u8)5
#define TIMER0_u8_EXTERNAL_CLK_SRC_FALLING_EDGE			(u8)6
#define TIMER0_u8_EXTERNAL_CLK_SRC_RISING_EDGE			(u8)7

/*
 * Timer 0 Modes
 * */

/*	Normal Mode */
#define TIMER0_u8_MODE_NORMAL							(u8)0x00

/*	CTC Modes	*/
#define TIMER0_u8_MODE_CTC_PIN_DISCONNECTED				(u8)0x08
#define TIMER0_u8_MODE_CTC_TOGGLE_PIN					(u8)0x18
#define TIMER0_u8_MODE_CTC_CLEAR_PIN					(u8)0x28
#define TIMER0_u8_MODE_CTC_SET_PIN						(u8)0x38

/*	Fast PWM	*/
#define TIMER0_u8_FAST_PWM_PIN_DISCONNECTED				(u8)0x48
#define TIMER0_u8_FAST_PWM_NON_INVERTING				(u8)0x68
#define TIMER0_u8_FAST_PWM_INVERTING					(u8)0x78

/*	Phase Correct PWM	*/
#define TIMER0_u8_PHASE_CORRECT_PWM_PIN_DISCONNECTED	(u8)0x40
#define TIMER0_u8_PHASE_CORRECT_PWM_NON_INVERTING		(u8)0x60
#define TIMER0_u8_PHASE_CORRECT_PWM_INVERTING			(u8)0x70




/*
 * ISRs for Timer 0
 * */

/*	Timer 0 ISR for Compare Match Event		*/
void __vector_10(void) __attribute__ ((signal));
/*	Timer 0 ISR for Overflow Event			*/
void __vector_11(void) __attribute__ ((signal));


#endif /* TIMER_PRIVATE_H_ */
