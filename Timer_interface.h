/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Module  :			Timer								*/
/*   Version :          V01	                                */
/*	 File	 :			interfaces.h						*/
/*                                                          */
/* ******************************************************** */
#ifndef TIMER_INTERFACE_H_
#define TIMER_INTERFACE_H_

#define SYSTEM_CLK										(f64)8000000.0


#define TIMER_u8_TIMER0_INDEX							(u8)0
#define TIMER_u8_TIMER1_INDEX							(u8)1
#define TIMER_u8_TIMER2_INDEX							(u8)2


/*	Timer 0 Interrupt Events					*/
#define TIMER0_u8_COMPARE_MATCH_INTERRUPT				(u8)0x02
#define TIMER0_u8_OVERFLOW_INTERRUPT					(u8)0x01
#define TIMER0_u8_BOTH_INTERRUPTS						(u8)0x03

#define TIMER2_u8_COMPARE_MATCH_INTERRUPT				(u8)0x40
#define TIMER2_u8_OVERFLOW_INTERRUPT					(u8)0x80
#define TIMER2_u8_BOTH_INTERRUPTS						(u8)0xC0


#define TIMER_u8_STATE_ENABLED							(u8)50
#define TIMER_u8_STATE_DISABLED							(u8)60

typedef void (*CallbackFunctionPtrType) (void);

void Timer_voidInit(void);
u8 Timer_u8Enable(u8 Copy_u8TimerIndex);
u8 Timer_u8Disable(u8 Copy_u8TimerIndex);
u8 Timer_u8SetCallback(u8 Copy_u8TimerInterruptEvent, CallbackFunctionPtrType Copy_Pu8CallbackFunction);
u8 Timer_u8SetDesiredTimeMilliSecond(u8 Copy_u8TimerIndex, f64 Copy_u8DesiredTimeMilliSecond);
u8 Timer_u8SetDesiredTimeMicroSecond(u8 Copy_u8TimerIndex, f64 Copy_u8DesiredTimeMicroSecond);


#endif /* TIMER_INTERFACE_H_ */
