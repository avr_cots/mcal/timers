/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Module  :			Timer								*/
/*   Version :          V01	                                */
/*	 File	 :			prog.c								*/
/*                                                          */
/* ******************************************************** */





#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "Timer_interface.h"
#include "Timer_private.h"
#include "Timer_config.h"

/*	Definition of a pointer to function type
 * 	The function takes void and returns void	*/


u32 Timer0Counter, Timer0OverflowCounts;
u8 	Timer0Preload;

u32 Timer_u8Levels[3] = {256, 65536, 256};

/*	Creating arrays of callback functions		*/
CallbackFunctionPtrType Timer_PAOverflowCallbackFunctions[TIMER_u8_NO_OF_TIMERS];
CallbackFunctionPtrType Timer_PACompareMatchCallbackFunctions[TIMER_u8_NO_OF_TIMERS];

/*	Timer 0 ISR for Compare Match Event		*/
void __vector_10(void)
{
	if(Timer_PACompareMatchCallbackFunctions[TIMER_u8_TIMER0_INDEX] != NULL)
	{
		Timer0Counter++;
		if(Timer0Counter == Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].OverflowsCount)
		{
			/*	Calling the Callback Function	*/
			Timer_PACompareMatchCallbackFunctions[TIMER_u8_TIMER0_INDEX]();

			Timer0Counter = 0;

			/*	Setting the preload value for Timer 0					*/
			TIMER_u8_TCNT0 = Timer_ASTimerConfigsArray[0].Preload;

		}
		/*	Clearing the Interrupt Flag		*/
		//SET_BIT(TIMER_u8_TIFR, 1);
	}
}
/*	Timer 0 ISR for Overflow Event			*/
void __vector_11(void)
{
	if(Timer_PAOverflowCallbackFunctions[TIMER_u8_TIMER0_INDEX] != NULL)
	{

		Timer0Counter++;
		if(Timer0Counter == Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].OverflowsCount)
		{
			/*	Calling the Callback Function	*/
			Timer_PAOverflowCallbackFunctions[TIMER_u8_TIMER0_INDEX]();

			Timer0Counter = 0;

			/*	Setting the preload value for Timer 0					*/
			TIMER_u8_TCNT0 = Timer_ASTimerConfigsArray[0].Preload;

		}


		/*	Clearing the Interrupt Flag		*/
		//SET_BIT(TIMER_u8_TIFR, 0);
	}
}


void Timer_voidInit(void)
{
	u8 Local_u8PWM_DutyCycle;

	/*	*********************************	*
	 *										*
	 * Initial Configurations for Timer 0	*
	 *										*
	 *  *********************************	*/

	/*	Configuring the mode for Timer 0							*/
	TIMER_u8_TCCR0 |= Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].Mode;

	/*	In case of CTC mode, setting the value of OCR0 				*/
	if((Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_MODE_CTC_PIN_DISCONNECTED) ||
	   (Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_MODE_CTC_TOGGLE_PIN) ||
	   (Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_MODE_CTC_CLEAR_PIN) ||
	   (Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_MODE_CTC_SET_PIN))
	{
		/*	*****************   TO BE MODIFIED   *****************	*/
		TIMER_u8_OCR0 = Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].CTC_value;
	}
	else if((Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_FAST_PWM_NON_INVERTING)||
			(Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_PHASE_CORRECT_PWM_NON_INVERTING))
	{
		Local_u8PWM_DutyCycle = (Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].PWM_DutyCycle /100.0);
		TIMER_u8_OCR0 = Local_u8PWM_DutyCycle * Timer_u8Levels[TIMER_u8_TIMER0_INDEX];
	}
	else if((Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_FAST_PWM_INVERTING) ||
			(Timer_ASTimerConfigsArray[0].Mode == TIMER0_u8_PHASE_CORRECT_PWM_INVERTING))
	{
		Local_u8PWM_DutyCycle = (Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].PWM_DutyCycle /100.0);
		TIMER_u8_OCR0 = Timer_u8Levels[TIMER_u8_TIMER0_INDEX] - (Local_u8PWM_DutyCycle * Timer_u8Levels[TIMER_u8_TIMER0_INDEX]);
	}


	/*	Setting the preload value for Timer 0							*/
	TIMER_u8_TCNT0 = Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].Preload;


	if(Timer_ASTimerConfigsArray[0].InitialState == TIMER_u8_STATE_ENABLED)
	{
		/*	Enabling/disabling interrupts for Timer 0					*/
		TIMER_u8_TIMSK |= Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].InterruptEvent;

		/*	Configuring the prescaler and enabling/disabling Timer 0	*/
		TIMER_u8_TCCR0 |= Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].Prescaler;
	}



	/*	Setting the callback functions for the ISRs of Timer 0 		*/
	Timer_PAOverflowCallbackFunctions[TIMER_u8_TIMER0_INDEX] = NULL;
	Timer_PACompareMatchCallbackFunctions[TIMER_u8_TIMER0_INDEX] = NULL;

	return;
}



u8 Timer_u8Enable(u8 Copy_u8TimerIndex)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_u8TimerIndex < TIMER_u8_NO_OF_TIMERS)
	{
		switch(Copy_u8TimerIndex)
		{
			case TIMER_u8_TIMER0_INDEX:
				/*	Enabling/disabling interrupts for Timer 0					*/
				TIMER_u8_TIMSK |= Timer_ASTimerConfigsArray[0].InterruptEvent;
				/*	Configuring the prescaler and enabling Timer 0				*/
				TIMER_u8_TCCR0 |= Timer_ASTimerConfigsArray[Copy_u8TimerIndex].Prescaler;
				Local_u8Error = STD_u8_OK;
				break;
			case TIMER_u8_TIMER1_INDEX:
				Local_u8Error = STD_u8_OK;
				break;
			case TIMER_u8_TIMER2_INDEX:
				Local_u8Error = STD_u8_OK;
				break;
		}

	}
	return Local_u8Error;
}


u8 Timer_u8Disable(u8 Copy_u8TimerIndex)
{
	u8 Local_u8Error = STD_u8_ERROR;

	if(Copy_u8TimerIndex < TIMER_u8_NO_OF_TIMERS)
	{
		switch(Copy_u8TimerIndex)
		{
			case TIMER_u8_TIMER0_INDEX:
				CLR_BIT(TIMER_u8_TCCR0, 0);
				CLR_BIT(TIMER_u8_TCCR0, 1);
				CLR_BIT(TIMER_u8_TCCR0, 2);
				Local_u8Error = STD_u8_OK;
				break;
			case TIMER_u8_TIMER1_INDEX:
				Local_u8Error = STD_u8_OK;
				break;
			case TIMER_u8_TIMER2_INDEX:
				Local_u8Error = STD_u8_OK;
				break;
		}

	}
	return Local_u8Error;
}



u8 Timer_u8SetCallback(u8 Copy_u8TimerInterruptEvent, CallbackFunctionPtrType Copy_Pu8CallbackFunction)
{
	u8 Local_u8Error = STD_u8_ERROR;

	switch(Copy_u8TimerInterruptEvent)
	{
		case TIMER0_u8_COMPARE_MATCH_INTERRUPT:
			Timer_PACompareMatchCallbackFunctions[TIMER_u8_TIMER0_INDEX] = Copy_Pu8CallbackFunction;
			break;
		case TIMER0_u8_OVERFLOW_INTERRUPT:
			Timer_PAOverflowCallbackFunctions[TIMER_u8_TIMER0_INDEX] = Copy_Pu8CallbackFunction;
			break;
	}

	return Local_u8Error;
}



u8 Timer_u8SetDesiredTimeMilliSecond(u8 Copy_u8TimerIndex, f64 Copy_u8DesiredTimeMilliSecond)
{
	u8 Local_u8Error = STD_u8_ERROR;
	f64 Local_f64TickTime, Local_f64OverflowCounts, Local_f64OverflowCountsFloatingPart;
	u32 Local_u32OverflowCountsIntegerPart;
	u8 	Local_u8PreloadValue;
	if(Copy_u8TimerIndex < TIMER_u8_NO_OF_TIMERS)
	{
		/*	Calculating the Tick Time			*/
		Local_f64TickTime = (Timer_u8Levels[Copy_u8TimerIndex] * Timer_ASTimerConfigsArray[Copy_u8TimerIndex].Prescaler) / SYSTEM_CLK;
		/*	Calculating the Overflow Counts		*/
		Local_f64OverflowCounts = (Copy_u8DesiredTimeMilliSecond/1000.0) / Local_f64TickTime;

		/*	Splitting the Overflow Counts into two parts; an integer part, and a floating part		*/
		Local_u32OverflowCountsIntegerPart = (u32)Local_f64OverflowCounts;
		Local_f64OverflowCountsFloatingPart = Local_f64OverflowCounts - (u32)Local_f64OverflowCounts;

		/*	Incrementing the Integer Part if the Overflow Count is a floating number */
		/*if(Local_f64OverflowCountsFloatingPart != 0.0)
		{
			Local_u32OverflowCountsIntegerPart ++;
		}*/
		/*	Calculating the Preload Value		*/
		Local_u8PreloadValue = Timer_u8Levels[Copy_u8TimerIndex] - (Local_f64OverflowCountsFloatingPart * Timer_u8Levels[Copy_u8TimerIndex]);

		Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].Preload = Local_u8PreloadValue;
		Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].OverflowsCount = Local_u32OverflowCountsIntegerPart;
		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}

u8 Timer_u8SetDesiredTimeMicroSecond(u8 Copy_u8TimerIndex, f64 Copy_u8DesiredTimeMicroSecond)
{
	u8 Local_u8Error = STD_u8_ERROR;
	f64 Local_f64TickTime, Local_f64OverflowCounts, Local_f64OverflowCountsFloatingPart;
	u32 Local_u32OverflowCountsIntegerPart;
	u8 	Local_u8PreloadValue;
	if(Copy_u8TimerIndex < TIMER_u8_NO_OF_TIMERS)
	{
		/*	Calculating the Tick Time			*/
		Local_f64TickTime = (Timer_u8Levels[Copy_u8TimerIndex] * Timer_ASTimerConfigsArray[Copy_u8TimerIndex].Prescaler) / SYSTEM_CLK;
		/*	Calculating the Overflow Counts		*/
		Local_f64OverflowCounts = (Copy_u8DesiredTimeMicroSecond/1000000.0) / Local_f64TickTime;

		/*	Splitting the Overflow Counts into two parts; an integer part, and a floating part		*/
		Local_u32OverflowCountsIntegerPart = (u32)Local_f64OverflowCounts;
		Local_f64OverflowCountsFloatingPart = Local_f64OverflowCounts - (u32)Local_f64OverflowCounts;

		/*	Incrementing the Integer Part if the Overflow Count is a floating number */
		/*if(Local_f64OverflowCountsFloatingPart != 0.0)
		{
			Local_u32OverflowCountsIntegerPart ++;
		}*/
		/*	Calculating the Preload Value		*/
		Local_u8PreloadValue = Timer_u8Levels[Copy_u8TimerIndex] - (Local_f64OverflowCountsFloatingPart * Timer_u8Levels[Copy_u8TimerIndex]);

		Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].Preload = Local_u8PreloadValue;
		Timer_ASTimerConfigsArray[TIMER_u8_TIMER0_INDEX].OverflowsCount = Local_u32OverflowCountsIntegerPart;
		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}

